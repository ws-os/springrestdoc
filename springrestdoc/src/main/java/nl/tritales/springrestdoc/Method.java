/*
 * Copyright 2014-2015 Wim van Haaren (wim@tritales.nl) All Rights Reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nl.tritales.springrestdoc;

import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMethod;

class Method {

    private static final String DEFAULT_HTTP_METHOD = "GET";
    private static final String DEFAULT_CONSUMES_MEDIATYPE = MediaType.APPLICATION_JSON_VALUE;
    private static final String DEFAULT_PRODUCES_MEDIATYPE = MediaType.APPLICATION_JSON_VALUE;

    private final String description;
    private final Set<RequestMethod> verbs = new TreeSet<>();
    private final Set<String> uris;
    private final SortedSet<String> consumes = new TreeSet<>();
    private final SortedSet<String> produces = new TreeSet<>();
    private final String defaultHttpStatus;
    private final List<RestParam> parameters;
    private final String returns;

    public Method(final String description, final Set<RequestMethod> requestMethods, final Set<String> uriPatterns,
            final Set<MediaType> consumableMediaTypes, final Set<MediaType> producibleMediaTypes,
            final HttpStatus defaultHttpStatus, final List<RestParam> restParameters, final String returns) {
        this.description = description;
        if (requestMethods != null && requestMethods.size() > 0) {
            this.verbs.addAll(requestMethods);
        } else {
            this.verbs.add(RequestMethod.GET);
        }
        this.uris = uriPatterns;
        this.returns = returns;
        if (consumableMediaTypes.size() > 0) {
            for (final MediaType mediaType : consumableMediaTypes) {
                this.consumes.add(mediaType.toString());
            }
        } else {
            this.consumes.add(DEFAULT_CONSUMES_MEDIATYPE);
        }
        if (producibleMediaTypes.size() > 0) {
            for (final MediaType mediaType : producibleMediaTypes) {
                this.produces.add(mediaType.toString());
            }
        } else {
            this.produces.add(DEFAULT_PRODUCES_MEDIATYPE);
        }
        if (defaultHttpStatus != null) {
            this.defaultHttpStatus = defaultHttpStatus.value() + " " + defaultHttpStatus.getReasonPhrase();
        } else {
            this.defaultHttpStatus = null;
        }
        this.parameters = restParameters;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();

        if (verbs.size() > 0) {
            sb.append(StringUtils.join(verbs, " "));
        } else {
            sb.append(DEFAULT_HTTP_METHOD);
        }

        sb.append(" " + StringUtils.join(uris, " "));

        sb.append(" consumes: ");
        final String allConsumableMediaTypes = StringUtils.join(consumes, " ");
        sb.append(allConsumableMediaTypes);

        sb.append(" produces: ");
        String allProducibleMediaTypes = DEFAULT_PRODUCES_MEDIATYPE;
        if (produces.size() > 0) {
            allProducibleMediaTypes = StringUtils.join(produces, " ");
        }
        sb.append(allProducibleMediaTypes);

        sb.append(" default HTTP response status: " + defaultHttpStatus);

        return sb.toString();
    }

    public Set<RequestMethod> getVerbs() {
        return verbs;
    }

    public Set<String> getUris() {
        return uris;
    }

    public SortedSet<String> getConsumes() {
        return consumes;
    }

    public SortedSet<String> getProduces() {
        return produces;
    }

    public String getDefaultHttpStatus() {
        return defaultHttpStatus;
    }

    public List<RestParam> getParameters() {
        return parameters;
    }

    public String getReturns() {
        return returns;
    }

    public String getDescription() {
        return description;
    }

}
