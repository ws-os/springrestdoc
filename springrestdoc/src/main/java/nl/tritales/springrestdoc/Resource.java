/*
 * Copyright 2014 Wim van Haaren (wim@tritales.nl) All Rights Reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nl.tritales.springrestdoc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.thoughtworks.qdox.JavaProjectBuilder;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaSource;

public class Resource implements Comparable<Resource> {

    private final String name;

    private final String uri;

    String description;

    final Class<?> declaringClass;

    final List<Method> methods = new ArrayList<>();

    public Resource(final String uri, final Class<?> declaringClass, final File sourceDir) {
        this.name = splitOnDashAndCapitalize(StringUtils
                .capitalize(StringUtils.substring(uri, uri.lastIndexOf("/") + 1)));
        this.uri = uri;
        this.declaringClass = declaringClass;
        if (sourceDir != null) {
            parseJDoc(declaringClass, sourceDir);
        }
    }

    private String splitOnDashAndCapitalize(final String name) {
        if (!StringUtils.contains(name, "-")) {
            return name;
        }
        final String[] parts = StringUtils.split(name, "-");
        final StringBuilder sb = new StringBuilder();
        for (final String part : parts) {
            sb.append(StringUtils.capitalize(part));
        }
        return sb.toString();
    }

    private void parseJDoc(final Class<?> declaringClass, final File sourceDir) {
        try {
            final JavaProjectBuilder builder = new JavaProjectBuilder();
            final File file = new File(sourceDir, declaringClass.getCanonicalName().replace(".", "/") + ".java");
            final JavaSource src = builder.addSource(file);

            final JavaClass jc = src.getClasses().get(0);
            this.description = jc.getComment();
        } catch (final FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (final IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public String getName() {
        return name;
    }

    public String getUri() {
        return uri;
    }

    public Class<?> getRestController() {
        return declaringClass;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((declaringClass == null) ? 0 : declaringClass.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((uri == null) ? 0 : uri.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Resource other = (Resource) obj;
        if (declaringClass == null) {
            if (other.declaringClass != null) {
                return false;
            }
        } else if (!declaringClass.equals(other.declaringClass)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (uri == null) {
            if (other.uri != null) {
                return false;
            }
        } else if (!uri.equals(other.uri)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Resource [name=" + name + ", uri=" + uri + ", declaringClass=" + declaringClass.getPackage().getName()
                + "." + declaringClass.getName() + "]";
    }

    @Override
    public int compareTo(final Resource o) {
        return this.name.compareTo(o.name);
    }

    public List<Method> getMethods() {
        return methods;
    }

    public void add(final Method method) {
        this.getMethods().add(method);
    }

}
